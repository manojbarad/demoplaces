//
//  ValidationResult.swift
//  MyPlacesApp
//
//  Created by Manoj on 11/13/20.
//

import Foundation

struct ValidationResult{
    let success: Bool
    let errorMessage: String?
}
