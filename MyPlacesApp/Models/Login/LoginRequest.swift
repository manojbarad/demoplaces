//
//  LoginRequest.swift
//  MyPlacesApp
//
//  Created by Manoj on 10/31/20.
//

import Foundation

struct LoginRequest : Encodable {
    let userEmail, userPassword: String
}
